
 <!-- Header -->
 <!-- He call CSS  -->
 
 <?php include __DIR__ . '/../../../assets/indexLayout/header.php'; ?>
 <!-- /Header -->
<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <!-- /Preloader -->
    <div id="wrapper">
        <!-- Navigation -->
        <!-- He call Header  -->
         <?php include __DIR__ . '/../../../assets/indexLayout/navigation.php'; ?>
        <!--/ Navigation -->
        <!-- Left navbar-header -->
        <!-- He call Navigation -->
         <?php include __DIR__ . '/../../../assets/indexLayout/leftbar.php'; ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page info  -->
                <?php include __DIR__ . '/../../../assets/indexLayout/page_info.php'; ?>
                <!-- /Page info  -->
                <!--Dasboard body -->
                <?php include __DIR__ . '/../../../assets/indexLayout/dashboard.php'; ?>
                <!--/Dasboard body -->
                <!-- .right-sidebar -->
                <?php include __DIR__ . '/../../../assets/indexLayout/rightbar.php'; ?>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <!-- Footer -->
            <?php include __DIR__ . '/../../../assets/indexLayout/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <!-- All Java script that are used by this template  -->
    <?php include __DIR__ . '/../../../assets/indexLayout/js.php'; ?>
    <!-- /jQuery -->
</body>
</html>