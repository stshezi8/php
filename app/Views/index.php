 <!-- Header -->
 <!-- He call CSS  -->
<?php include 'indexLayout/header.php'; ?>
 <!-- /Header -->
<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <!-- /Preloader -->
    <div id="wrapper">
        <!-- Navigation -->
        <!-- He call Header  -->
         <?php include 'indexLayout/navigation.php'; ?>
        <!--/ Navigation -->
        <!-- Left navbar-header -->
        <!-- He call Navigation -->
         <?php include 'indexLayout/leftbar.php'; ?>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page info  -->
                <?php include 'indexLayout/page_info.php'; ?>
                <!-- /Page info  -->
                <!--Dasboard body -->
                <?php include 'indexLayout/dashboard.php'; ?>
                <!--/Dasboard body -->
                <!-- .right-sidebar -->
                <?php include 'indexLayout/rightbar.php'; ?>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <!-- Footer -->
            <?php include 'indexLayout/footer.php'; ?>
            <!-- /Footer -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <!-- All Java script that are used by this template  -->
    <?php include 'indexLayout/js.php'; ?>
    <!-- /jQuery -->
</body>
</html>