# CodeIgniter 4 Development

School Management Application
## What is CodeIgniter?

CodeIgniter is a PHP full-stack web framework that is light, fast, flexible and secure.
More information can be found at the [official site](https://codeigniter.com).

This repository holds the source code for CodeIgniter 4 only.
Version 4 is a complete rewrite to bring the quality and the code into a more modern version,
while still keeping as many of the things intact that has made people love the framework over the years.

More information about the plans for version 4 can be found in [CodeIgniter 4](https://forum.codeigniter.com/forumdisplay.php?fid=28) on the forums.

### Documentation

The [User Guide](https://codeigniter4.github.io/userguide/) is the primary documentation for CodeIgniter 4.

The current **in-progress** User Guide can be found [here](https://codeigniter4.github.io/CodeIgniter4/).
As with the rest of the framework, it is a work in progress, and will see changes over time to structure, explanations, etc.

You might also be interested in the [API documentation](https://codeigniter4.github.io/api/) for the framework components.

## Important Change with index.php

index.php is no longer in the root of the project! It has been moved inside the *public* folder,
for better security and separation of components.

This means that you should configure your web server to "point" to your project's *public* folder, and
not to the project root. A better practice would be to configure a virtual host to point there. A poor practice would be to point your web server to the project root and expect to enter *public/...*, as the rest of your logic and the
framework are exposed.

**Please** read the user guide for a better explanation of how CI4 works!

## Project Dependencies

[Download Xampp](https://www.apachefriends.org/download.html).

[Download Codeigniter](https://www.codeigniter.com/)

[stackoverflow Link](https://stackoverflow.com/questions/72877565/failed-to-run-php-spark-serve-can-anyone-solve-the-problem) 

[Download Codeigniter On Gitlab](https://github.com/codeigniter4/CodeIgniter4/releases/tag/v4.4.50)

**Run the project** php spark serve 

[Download Template](https://optimumlinkup.com.ng/html.zip)



## Error 
Whoops! We seem to have hit a snag. Please try again later...
## Solution : 
[stackoverflow](https://stackoverflow.com/questions/60558157/why-whoops-error-showing-in-codeigniter-4)
CI_ENVIRONMENT = development
